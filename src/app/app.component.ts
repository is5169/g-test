import {Component} from "@angular/core";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent  {
  showVideo = false;
  statusText = 'Статус не определен';
  timer = 3_000;

  onTouchstart(e: Event): void {
    e.preventDefault();
    e.stopPropagation();
    this.showVideo = true;
    this.scanStatusProcess();
  }

  scanStatusProcess(): void {
    this.statusText = 'Сканирование...';

    setTimeout(() => {
      this.statusText = 'Анализ данных...';

      setTimeout(() => {
        this.statusText = 'Готово: ты Пидор!';

      }, this.timer);
    }, this.timer);
  }
}
